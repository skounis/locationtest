package me.about.kounis.stavros.LocationTest;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import course.examples.Location.GetLocation.R;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.widget.TextView;

public class LocationTestActivity extends Activity {

	private static final long ONE_MIN = 1000 * 60;
	private static final long TWO_MIN = ONE_MIN * 2;
	private static final long FIVE_MIN = ONE_MIN * 5;
	private static final long MEASURE_TIME = 1000 * 30;
	private static final long POLLING_FREQ = 1000 * 10;
	private static final float MIN_ACCURACY = 25.0f;
	private static final float MIN_LAST_READ_ACCURACY = 500.0f;
	private static final float MIN_DISTANCE = 0.1f; // 10.0f;

	// Views for display location information
	private TextView mAccuracyView;
	private TextView mTimeView;
	private TextView mLatView;
	private TextView mLngView;

	private TextView mNetworkAccuracyView;
	private TextView mNetworkTimeView;
	private TextView mNetworkLatView;
	private TextView mNetworkLngView;

	// Reference to the LocationManager and LocationListener
	private LocationManager mLocationManager;
	private LocationListener mGPSLocationListener;
	private LocationListener mNetworkLocationListener;

	private final String TAG = "LocationGetLocationActivity";
	private final String TAG_NET = "LocationGetLocationActivity-NET";
	private final String TAG_GPS = "LocationGetLocationActivity-GPS";

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.main);

		mAccuracyView = (TextView) findViewById(R.id.accuracy_view);
		mTimeView = (TextView) findViewById(R.id.time_view);
		mLatView = (TextView) findViewById(R.id.lat_view);
		mLngView = (TextView) findViewById(R.id.lng_view);

		mNetworkAccuracyView = (TextView) findViewById(R.id.net_accuracy_view);
		mNetworkTimeView = (TextView) findViewById(R.id.net_time_view);
		mNetworkLatView = (TextView) findViewById(R.id.net_lat_view);
		mNetworkLngView = (TextView) findViewById(R.id.net_lng_view);

		// Acquire reference to the LocationManager
		if (null == (mLocationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE)))
			finish();

		updateDisplayGPS(lastKnownLocationForProvider(LocationManager.GPS_PROVIDER));
		updateDisplayNetwork(lastKnownLocationForProvider(LocationManager.NETWORK_PROVIDER));

		mGPSLocationListener = new LocationListener() {

			// Called back when location changes

			public void onLocationChanged(Location location) {

				// Determine whether new location is better than current best
				// estimate

				// Update display
				updateDisplayGPS(location);

			}

			public void onStatusChanged(String provider, int status,
					Bundle extras) {
				// NA
			}

			public void onProviderEnabled(String provider) {
				// NA
			}

			public void onProviderDisabled(String provider) {
				// NA
			}
		};

		/* First field test end ups with no networks location updates. 
		 * 
		 * Some similar mentions:
		 * http://stackoverflow.com/questions/13594932/network-provider-not-providing-updated-locations
		 */
		mNetworkLocationListener = new LocationListener() {

			@Override
			public void onStatusChanged(String provider, int status,
					Bundle extras) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onProviderEnabled(String provider) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onProviderDisabled(String provider) {
				// TODO Auto-generated method stub

			}

			@Override
			public void onLocationChanged(Location location) {
				// Update display
				updateDisplayNetwork(location);
			}
		};
	}

	@Override
	protected void onResume() {
		super.onResume();

		// Determine whether initial reading is
		// "good enough"

		// Register for network location updates
		mLocationManager.requestLocationUpdates(
				LocationManager.NETWORK_PROVIDER, POLLING_FREQ, MIN_DISTANCE,
				mNetworkLocationListener);

		// Register for GPS location updates
		mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
				POLLING_FREQ, MIN_DISTANCE, mGPSLocationListener);
	}

	// Unregister location listeners

	@Override
	protected void onPause() {
		super.onPause();

		mLocationManager.removeUpdates(mGPSLocationListener);
		mLocationManager.removeUpdates(mNetworkLocationListener);

	}

	// Update display
	private void updateDisplayGPS(Location location) {

		if (location == null)
			return;

		String msg = location.getAccuracy()
				+ ", "
				+ new SimpleDateFormat("MM/dd/yyyy HH:mm:ss",
						Locale.getDefault())
						.format(new Date(location.getTime())) + ", "
				+ location.getLongitude() + ", " + location.getLatitude();

		Log.i(TAG_GPS, msg);
		
		appendLog(TAG_GPS + ", " + msg);

		mAccuracyView.setText("Accuracy:" + location.getAccuracy());

		mTimeView.setText("Time:"
				+ new SimpleDateFormat("MM/dd/yyyy HH:mm:ss", Locale
						.getDefault()).format(new Date(location.getTime())));

		mLatView.setText("Longitude:" + location.getLongitude());

		mLngView.setText("Latitude:" + location.getLatitude());

	}

	private void updateDisplayNetwork(Location location) {

		if (location == null)
			return;

		String msg = location.getAccuracy()
				+ ", "
				+ new SimpleDateFormat("MM/dd/yyyy HH:mm:ss",
						Locale.getDefault())
						.format(new Date(location.getTime())) + ", "
				+ location.getLongitude() + ", " + location.getLatitude();

		Log.i(TAG_NET, msg);
		
		appendLog(TAG_NET + ", " + msg);

		mNetworkAccuracyView.setText("Accuracy:" + location.getAccuracy());

		mNetworkTimeView.setText("Time:"
				+ new SimpleDateFormat("MM/dd/yyyy HH:mm:ss", Locale
						.getDefault()).format(new Date(location.getTime())));

		mNetworkLatView.setText("Longitude:" + location.getLongitude());

		mNetworkLngView.setText("Latitude:" + location.getLatitude());

	}

	// Get the last known location from all providers
	// return best reading that is as accurate as minAccuracy and
	// was taken no longer then minAge milliseconds ago. If none,
	// return null.

	private Location lastKnownLocationForProvider(String provider) {
		
		Location location = mLocationManager.getLastKnownLocation(provider);
		
		if (location != null){
			String msg = location.getAccuracy()
					+ ", "
					+ new SimpleDateFormat("MM/dd/yyyy HH:mm:ss",
							Locale.getDefault())
							.format(new Date(location.getTime())) + ", "
					+ location.getLongitude() + ", " + location.getLatitude();
			
			appendLog(provider + ", " + msg);		
		}

		return location;
	}

	public void appendLog(String text) {
	
		Log.i(TAG, "Path: " + Environment.getExternalStorageDirectory());
		
		File logFile = new File(Environment.getExternalStorageDirectory(), "log.file");
		
		if (!logFile.exists()) {
			try {
				logFile.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		try {
			// BufferedWriter for performance, true to set append to file flag
			BufferedWriter buf = new BufferedWriter(new FileWriter(logFile,
					true));
			buf.append(text);
			buf.newLine();
			buf.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}